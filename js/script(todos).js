//Matriz para tablero
var myarray=[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
var monumento = false;
var juegot = false;
var edificio = "";
var edificioCreado = false;
var hayEdificio = false;
var contadorBoton = 0;
var contador = 0;
var recursoSeleccionado = false;
var monumentoElegido;
var hayEdificioGris = false;
var hayEdificioAzul = false;
var hayEdificioNaranja = false;
var hayEdificioAmarrillo = false;
var hayEdificioVerde = false;
var hayEdificioChillante = false;
var hayEdificioNegro = false;
var hayEdificioLila = false;
var edificioParaC = 0;
var monumentoCreado = false;
var validarEdificio = false;
//FUNCIONES
    //Selecciona aleatoriamente un numero que sera asigando a un material
    function vMaterial(){
        var valorMaterial = Math.floor(Math.random()*5+1);
        return valorMaterial;
    }
    window.onload = function mostrarCartas(){
        document.getElementById('azul').style.backgroundImage = "url('./img/tarjetas/cottage.png')";
        document.getElementById('gris').style.backgroundImage = "url('./img/tarjetas/well.png')";
        document.getElementById('naranja').style.backgroundImage = "url('./img/tarjetas/farm.png')";
        document.getElementById('amarilla').style.backgroundImage = "url('./img/tarjetas/abbey.png')";
        document.getElementById('verde').style.backgroundImage = "url('./img/tarjetas/tavern.png')";
        document.getElementById('chillante').style.backgroundImage = "url('./img/tarjetas/bakery.png')";
        document.getElementById('negra').style.backgroundImage = "url('./img/tarjetas/trading-post.png')";
    }
        //Muestra las primeras tres cartas de materiales (aleatorio)
        function inicializarCartas(){
            if(monumento === true){
        if(contadorBoton > 0){
            alert('La partida ya ha iniciado');
        }
        else{
            for(let i=1; i<=3; i++){
                switch(vMaterial()){
                    case 1:
                        document.getElementById(i).style.backgroundImage = "url('./img/recursos/1.png')";
                        document.getElementById(i).value= '1';
                        break;
                    case 2:
                        document.getElementById(i).style.backgroundImage = "url('./img/recursos/2.png')";
                        document.getElementById(i).value= '2';
                        break;
                    case 3:
                        document.getElementById(i).style.backgroundImage = "url('./img/recursos/3.png')";
                        document.getElementById(i).value= '3';
                        break;
                    case 4:
                        document.getElementById(i).style.backgroundImage = "url('./img/recursos/4.png')";
                        document.getElementById(i).value= '4';
                        break;
                    case 5:
                        document.getElementById(i).style.backgroundImage = "url('./img/recursos/5.png')";
                        document.getElementById(i).value= '5';
                        break;
                }
                //document.getElementById(i).style.backgroundImage = url(recurso);
            }
            cartasEnPartida();
            contadorBoton++;
            document.getElementById('mon1').disabled = true;
            document.getElementById('mon2').disabled = true;
        }
    }
    else{
        alert('Elige un monumento para iniciar la partida');
    }   
        
    }
    
        //Seleccionar el material al azar
    function mandarCarta(id){
        if(juegot === false){
            var carta = document.getElementById(id).value;
            document.getElementById('Carta').value = carta;
            document.getElementById('boton').value = id;
            elegirMonumentos();
            recursoSeleccionado = true;
        }
    }
        //Llenar los campos con recursos
    function llenarCampo(x, y, id){
        if(juegot === false){
            var carta = document.getElementById('Carta').value;
        var botonCarta = document.getElementById('boton').value;
        var botonTabla = document.getElementById(id);
        if(recursoSeleccionado === true && edificioCreado === false){
            if(myarray[x][y] === 0){
                myarray[x][y] = parseInt(carta);
                switch(parseInt(carta)){
                    case 1:
                        botonTabla.src = './img/tablero/madera.png';
                        break;
                    case 2:
                        botonTabla.src = './img/tablero/trigo.png';
                        break;
                    case 3:
                        botonTabla.src = './img/tablero/ladrillo.png';
                        break;
                    case 4:
                        botonTabla.src = './img/tablero/vidrio.png';
                        break;
                    case 5:
                        botonTabla.src = './img/tablero/piedra.png';
                        break;
                }
                cambiarCarta(botonCarta);
                recursoSeleccionado = false;
                juegoterminado();
            }
            else{
            alert('Lugar ocupado');
            }
        }
        else if (recursoSeleccionado === false){
            alert('Por favor elija un recurso');
        }
            cartasEnPartida();
            usarMonumento();
        }
        
    }
        //Cambia el valor de la carta
    function cambiarCarta(id){
        //var cambio = document.getElementById(id).innerHTML=vMaterial();
        var cambio = vMaterial();
        document.getElementById(id).style.backgroundImage = recursos(cambio);
        document.getElementById(id).value = cambio;
    }
    function recursos(numero){
        var imagen;
        switch(numero){
            case 1:
                imagen = "url('./img/recursos/1.png')";
                break;
            case 2:
                imagen = "url('./img/recursos/2.png')";
                break;
            case 3:
                imagen = "url('./img/recursos/3.png')";
                break;
            case 4:
                imagen = "url('./img/recursos/4.png')";
                break;
            case 5:
                imagen = "url('./img/recursos/5.png')";
                break;
        }
        return imagen;
    }

   //FUNCIONES EDIFICIOS Y MONUMENTOS
function well(){
    /*HORIZONTAL*/ //Ya funciona 
    for(let x=0; x<myarray.length; x++){
        for(let y=0; y<myarray.length-1; y++){
            if((myarray[x][y] === 1) && (myarray[x][(y+1)] === 5)){
                hayEdificioGris = true;
                edificio = 'Well';
                document.getElementById('gris').disabled = false;
                document.getElementById('gris').style.borderColor = '#cb3234';
            }
            if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 1)){
                hayEdificioGris = true;
                edificio = 'Well';
                document.getElementById('gris').disabled = false;
                document.getElementById('gris').style.borderColor = '#cb3234';
            }
        }
    }
    /*VERTICAL*/
    for(let y=0; y<myarray.length; y++){
        for(let x=0; x<myarray.length-1; x++){
            if((myarray[x][y] === 1) && (myarray[x+1][y] === 5)){
                hayEdificioGris = true;
                edificio = 'Well';
                document.getElementById('gris').disabled = false;
                document.getElementById('gris').style.borderColor = '#cb3234';
            }
            if((myarray[x][y] === 5) && (myarray[x+1][y] === 1)){
                hayEdificioGris = true;
                edificio = 'Well';
                document.getElementById('gris').disabled = false;
                document.getElementById('gris').style.borderColor = '#cb3234';
            }
        }
    }
}
function cottage(){
    var botonAzul = document.getElementById('azul');
    /*HORIZONTALES*/
    for(let x=0; x<myarray.length; x++){
        for(let y=0; y<myarray.length; y++){
            if(y<3){
                if((myarray[x][y] === 3) && (myarray[x][(y+1)] === 4)){
                    if(x>0){
                        if((myarray[(x-1)][(y+1)] === 2)){
                            hayEdificioAzul = true;
                            edificio = 'Cottage';
                            botonAzul.disabled = false;
                            botonAzul.style.borderColor = '#cb3234';
                        }
                    }
                    if(x<3){
                        if((myarray[(x+1)][(y+1)] === 2)){
                            hayEdificioAzul = true;
                            edificio = 'Cottage';
                            botonAzul.disabled = false;
                            botonAzul.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
            if(y>0){
                if((myarray[x][y] === 3) && (myarray[x][(y-1)] === 4)){
                    if(x>0){
                        if((myarray[(x-1)][(y-1)] === 2)){
                            hayEdificioAzul = true;
                            edificio = 'Cottage';
                            botonAzul.disabled = false;
                            botonAzul.style.borderColor = '#cb3234';
                        }
                    }
                    if(x<3){
                        if((myarray[(x+1)][(y-1)] === 2)){
                            hayEdificioAzul = true;
                            edificio = 'Cottage';
                            botonAzul.disabled = false;
                            botonAzul.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
        }
    }
    /*VERTICALES*/
    for(let y=0; y<myarray.length; y++){
        for(let x=0; x<myarray.length; x++){
            if(x>0){
                if((myarray[x][y] === 3) && (myarray[(x-1)][y] === 4)){
                    if(y < 3){
                        if((myarray[(x-1)][(y+1)] === 2)){
                            hayEdificioAzul = true;
                            edificio = 'Cottage';
                            botonAzul.disabled = false;
                            botonAzul.style.borderColor = '#cb3234';
                        }
                    }
                    if(y > 0){
                        if((myarray[(x-1)][(y-1)] === 2)){
                            hayEdificioAzul = true;
                            edificio = 'Cottage';
                            botonAzul.disabled = false;
                            botonAzul.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
            if(x < 3){
            if((myarray[x][y] === 3) && (myarray[(x+1)][y] === 4)){
                if(y < 3){
                    if((myarray[(x+1)][(y+1)] === 2)){
                        hayEdificioAzul = true;
                        edificio = 'Cottage';
                        botonAzul.disabled = false;
                        botonAzul.style.borderColor = '#cb3234';
                    }
                }
                if(y > 0){
                    if((myarray[(x+1)][(y-1)] === 2)){
                        hayEdificioAzul = true;
                        edificio = 'Cottage';
                        botonAzul.disabled = false;
                        botonAzul.style.borderColor = '#cb3234';
                    }
                }
            }
            }
        }
    }
}
function farm(){
    var botonNaranja = document.getElementById('naranja');
    /*HORIZONTALES*/
    for(let x=0; x<myarray.length; x++){
        for(let y=0; y<myarray.length-1; y++){
            if((myarray[x][y] === 2) && (myarray[x][(y+1)] === 2)){
                if(x<3){
                    if((myarray[(x+1)][y] === 1) && (myarray[(x+1)][(y+1)] === 1)){ //H1
                        hayEdificioNaranja = true;
                        edificio = 'Farm';
                        botonNaranja.disabled = false;
                        botonNaranja.style.borderColor = '#cb3234';
                    }
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 1) && (myarray[(x-1)][(y+1)] === 1)){
                        hayEdificioNaranja = true;
                        edificio = 'Farm';
                        botonNaranja.disabled = false;
                        botonNaranja.style.borderColor = '#cb3234';
                    }
                }
            }
        }
    }
    /*VERTICALES*/
    for(let y=0; y<myarray.length; y++){
        for(let x=0; x<myarray.length-1; x++){
            if((myarray[x][y] === 2) && (myarray[(x+1)][y] === 2)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 1) && (myarray[(x+1)][(y+1)] === 1)){ //V1
                        hayEdificioNaranja = true;
                        edificio = 'Farm';
                        botonNaranja.disabled = false;
                        botonNaranja.style.borderColor = '#cb3234';
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 1) && (myarray[(x+1)][(y-1)] === 1)){ //V2
                        hayEdificioNaranja = true;
                        edificio = 'Farm';
                        botonNaranja.disabled = false;
                        botonNaranja.style.borderColor = '#cb3234';
                    }
                }
                
            }
        }
    }
}
function abbey(){
    var botonAmarillo = document.getElementById('amarilla');
    /*HORIZONTALES*/
    for(let x=0; x<myarray.length; x++){
        for(let y=0; y<myarray.length; y++){
            if(y > 1){
                if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 5) && (myarray[x][(y-2)] === 3)){
                    if(x>0){
                        if ((myarray[(x-1)][y] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][y] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                    
                }
            }
            if(y < 2){
                if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 5) && (myarray[x][(y+2)] === 3)){
                    if(x>0){
                        if ((myarray[(x-1)][y] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][y] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
        }
            
    }
    /*VERTICALES*/
    for(let y=0; y<myarray.length; y++){
        for(let x=0; x<myarray.length; x++){
            if(x<2){
                if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 5) && (myarray[(x+2)][y] === 3)){
                    if(y < 3){
                        if((myarray[x][(y+1)] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
            if(x>1){
                if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 5) && (myarray[(x-2)][y] === 3)){
                    if(y < 3){
                        if((myarray[x][(y+1)] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 4)){
                            hayEdificioAmarrillo = true;
                            edificio = 'Abbey';
                            botonAmarillo.disabled = false;
                            botonAmarillo.style.borderColor = '#cb3234';
                        }
                    }
                    
                }
            }
        }
    }
}
function tavern(){
    var botonVerde = document.getElementById('verde');
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length-1; y++){
            if(y < 2){
                if((myarray[x][y] === 3) && (myarray[x][y+1] === 3)&& (myarray[x][y+2] === 4)){
                    hayEdificioVerde = true;
                    edificio = 'Tavern';
                    botonVerde.disabled = false;
                    botonVerde.style.borderColor = '#cb3234';
                }
                if((myarray[x][y] === 4) && (myarray[x][y+1] === 3)&& (myarray[x][y+2] === 3)){
                    hayEdificioVerde = true;
                    edificio = 'Tavern';
                    botonVerde.disabled = false;
                    botonVerde.style.borderColor = '#cb3234';
                }
            } 
        }       
    }
    //Vertical
    for(let y = 0; y < myarray.length; y++){
        for(let x = 0; x < myarray.length-1; x++){
            if(x < 2){
                if((myarray[x][y]) === 3 && (myarray[x+1][y] === 3)&& (myarray[x+2][y] === 4)){
                    hayEdificioVerde = true;
                    edificio = 'Tavern';
                    botonVerde.disabled = false;
                    botonVerde.style.borderColor = '#cb3234';
                }
                if((myarray[x][y] === 4) && (myarray[x+1][y] === 3)&& (myarray[x+2][y] === 3)){
                    hayEdificioVerde = true;
                    edificio = 'Tavern';
                    botonVerde.disabled = false;
                    botonVerde.style.borderColor = '#cb3234';
                }
            }
        }
    }
}
function backery(){
    var  botonChillante = document.getElementById('chillante');
    /*HORIZONTALES*/
    for(let x=0; x<myarray.length; x++){
        for(let y=0; y<myarray.length-1; y++){
            if(y < 2){
                if((myarray[x][y] === 3) && (myarray[x][(y+1)] === 4) && (myarray[x][(y+2)] === 3)){
                    if(x>0){
                        if((myarray[(x-1)][(y+1)] === 2)){
                            hayEdificioChillante = true;
                            edificio = 'Backery';
                            botonChillante.disabled = false;
                            botonChillante.style.borderColor = '#cb3234';
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][(y+1)] === 2)){
                            hayEdificioChillante = true;
                            edificio = 'Backery';
                            botonChillante.disabled = false;
                            botonChillante.style.borderColor = '#cb3234';
                        }
                    }
                    
                }
            }
            
        }
    }
    /*VERTICALES*/
    for(let y=0; y<myarray.length; y++){
        for(let x=0; x<myarray.length-1; x++){
            if(x < 2){
                if((myarray[x][y] === 3) && (myarray[(x+1)][y] === 4) && (myarray[(x+2)][y] === 3)){
                    if(y < 3){
                        if((myarray[(x+1)][(y+1)] === 2)){
                            hayEdificioChillante = true;
                            edificio = 'Backery';
                            botonChillante.disabled = false;
                            botonChillante.style.borderColor = '#cb3234';
                        }
                    }
                    if(y > 0){
                        if((myarray[(x+1)][(y-1)] === 2)){
                            hayEdificioChillante = true;
                            edificio = 'Backery';
                            botonChillante.disabled = false;
                            botonChillante.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
        }
    }
}
function tradingpost(){
    var botonNegro = document.getElementById('negra');
    /*HORIZONTALES*/
    for(let x=0; x<myarray.length; x++){
        for(let y=0; y<myarray.length; y++){
            if(y < 2){
                if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 1) && (myarray[x][(y+2)] === 3)){
                    if(x>0){
                        if((myarray[x-1][y] === 5) && (myarray[(x-1)][(y+1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y+1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
            if(y > 1){
                if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 1) && (myarray[x][(y-2)] === 3)){
                    if(x > 0){
                        if((myarray[(x-1)][y] === 5) && (myarray[(x-1)][(y-1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y-1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
        }
    }
    /*VERTICALES*/
    for(let y=0; y<myarray.length; y++){
        for(let x=0; x<myarray.length; x++){
            if(x < 2){
                if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 1) && (myarray[(x+2)][y] === 3)){
                    if(y < 3){
                        if((myarray[x][(y+1)] === 5) && (myarray[(x+1)][(y+1)] === 1)){
                            hayEdificioNegro = true;
                            dificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 5) && (myarray[(x+1)][(y-1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
            if(x > 1){  
                if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 1) && (myarray[(x-2)][y] === 3)){
                    if(y < 3){
                        if((myarray[x][(y+1)] === 5) && (myarray[(x-1)][(y+1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 5) && (myarray[(x-1)][(y-1)] === 1)){
                            hayEdificioNegro = true;
                            edificio = 'Trading';
                            botonNegro.disabled = false;
                            botonNegro.style.borderColor = '#cb3234';
                        }
                    }
                }
            }
        }
    }
}
function mandrasPlace(){
    var botonLila = document.getElementById('lila');
    /*HORIZONTALES*/
    if(monumentoCreado === false){
        for(let x = 0; x < myarray.length; x++){
            for(let y = 0; y < myarray.length-1; y++){
                if(x < 3 && y < 3){
                    if((myarray[x][y] === 2) && (myarray[x][y+1] === 4) && (myarray[x+1][y] === 3) && (myarray[x+1][y+1] === 1)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                    if((myarray[x][y] === 3) && (myarray[x][y+1] === 1) && (myarray[x+1][y] === 2) && (myarray[x+1][y+1] === 4)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                    if((myarray[x][y] === 4) && (myarray[x][y+1] === 2) && (myarray[x+1][y] === 1) && (myarray[x+1][y+1] === 3)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                    if((myarray[x][y] === 1) && (myarray[x][y+1] === 3) && (myarray[x+1][y] === 4) && (myarray[x+1][y+1] === 2)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                }
            }
        }
        /*VERTICALES*/
        for(let y = 0; y < myarray.length; y++){
            for(let x = 0; x < myarray.length-1; x++){
                if(x < 3 && y < 3){
                    if((myarray[x][y] === 2) && (myarray[x][y+1] === 3) && (myarray[x+1][y] === 4) && (myarray[x+1][y+1] === 1)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                    if((myarray[x][y] === 3) && (myarray[x][y+1] === 2) && (myarray[x+1][y] === 1) && (myarray[x+1][y+1] === 4)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                    if((myarray[x][y] === 4) && (myarray[x][y+1] === 1) && (myarray[x+1][y] === 2) && (myarray[x+1][y+1] === 3)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                    if((myarray[x][y] === 1) && (myarray[x][y+1] === 4) && (myarray[x+1][y] === 3) && (myarray[x+1][y+1] === 2)){
                        hayEdificioLila = true;
                        edificio = 'Mandras';
                        botonLila.disabled = false;
                        botonLila.style.borderColor = '#cb3234';
                    }
                }  
            }
        }
    } 
}
function cathedralOfCatherina(){
    var botonLila = document.getElementById('lila');
    /*HORIZONTALES*/
    if(monumentoCreado === false){
        for(let x=0; x<myarray.length; x++){
            for(let y=0; y<myarray.length; y++){
                if(y < 3){
                    if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 4)){
                        if(x > 0){
                            if((myarray[(x-1)][(y+1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                        if(x < 3){
                            if((myarray[(x+1)][(y+1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                    }
                }
                if(y > 0){
                    if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 4)){
                        if(x > 0){
                            if((myarray[(x-1)][(y-1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                        if(x < 3){
                            if((myarray[(x+1)][(y-1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                    }
                }
            }
        }
        /*VERTICALES*/
        for(let y=0; y<myarray.length; y++){
            for(let x=0; x<myarray.length; x++){
                if(x > 0){
                    if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 4)){
                        if(y < 3){
                            if((myarray[(x-1)][(y+1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                        if(y > 0){
                            if((myarray[(x-1)][(y-1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                    }
                }
                if(x < 3){
                    if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 4)){
                        if(y < 3){
                            if((myarray[(x+1)][(y+1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                        if(y > 0){
                            if((myarray[(x+1)][(y-1)] === 2)){
                                hayEdificioLila = true;
                                edificio = 'Cathedral';
                                botonLila.disabled = false;
                                botonLila.style.borderColor = '#cb3234';
                            }
                        }
                    }
                } 
            }
        }
    } 
}

function limpiarTablero(){
    for (let x = 0; x < myarray.length; x++) {
      for (let y=0; y < myarray.length; y++) {
        myarray[x][y] = 0;
       document.getElementById("c"+x+y).innerHTML = "-";
       }
    } 
    //contador = 0;
    //terminado = false;    
}
function elegirMonumentos(id){
    if(id === 1){
        cathedralOfCatherina();
        monumentoElegido = 'Cathedral';
        document.getElementById('lila').style.backgroundImage = "url('./img/tarjetas/caterina.png')";
        monumento = true;
        usarMonumento(1);
    }
    if(id === 2){
        mandrasPlace();
        monumentoElegido = 'Mandras';
        document.getElementById('lila').style.backgroundImage = "url('./img/tarjetas/mandras.png')";
        monumento = true;
        usarMonumento(2);
    }
}
function cartasEnPartida(){
    cottage();
    well();
    farm();
    abbey();
    tavern();
    backery();
    tradingpost();
}
function juegoterminado(){
    var contador = 0;
    for(let k = 0; k < myarray.length; k++){
        for(let j = 0; j < myarray.length; j++){
            if(myarray[k][j] != 0){
                contador++;
            }
        }
    }
    if(contador === 16){
        if(hayEdificioGris === false && hayEdificioAmarrillo === false && hayEdificioAzul === false && hayEdificioChillante === false && hayEdificioGris === false && hayEdificioLila === false && hayEdificioNaranja === false && hayEdificioNegro === false && hayEdificioVerde === false){
            juegot = true;
            alert('El juego ya ha terminado');
            contabilizar();
        }
    }  
}
function mensajeEdificio(id){
    let mensaje = confirm('\u00bfDeseas crear el edificio?');
    if (mensaje){
        alert("Elige la celda en donde lo quieras crear");
        switch(id){
            case 1:
              edificioParaC = 1;
              break;
            case 2:
                edificioParaC = 2;
                break;
            case 3:
                edificioParaC = 3;
                break;
            case 4:
                edificioParaC = 4;
                break;
            case 5:
                edificioParaC = 5;
                break;
            case 6:
                edificioParaC = 6;
                break;
            case 7:
                edificioParaC = 7;
                break;            
        }
        contador = 0;
        edificioCreado = true;
    }else{
        edificioCreado=false;
    }   
}
function crearEdificio(x, y, id){
    validarEdificio = false;
    var tablero = './img/tablero/normal.png';
    var well = './img/tablero/well.png';
    var cottage = './img/tablero/cottage.png';
    var farm = './img/tablero/farm.png';
    var abbey = './img/tablero/abbey.png';
    var tavern = './img/tablero/tavern.png';
    var bakery = './img/tablero/bakery.png';
    var trading = './img/tablero/trading-post.png';
    var mandras = './img/tablero/mandras.png';
    var cathedral = './img/tablero/cathedral.png';
    if(edificioCreado === true){
        /*Edificios*/
        if(edificioParaC === 2){//TERMINADO
            if(y < 3){
            //Agarra 1 (1-5)
                if((myarray[x][y] === 1) && (myarray[x][(y+1)] === 5)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x][y+1] = 0;
                document.getElementById('c'+x+(y+1)).src = tablero;
                valoresBooleanos();
            }
            //Agarra 5 (5-1)
            if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 1)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x][y+1] = 0;
                document.getElementById('c'+x+(y+1)).src = tablero;
                valoresBooleanos();
            }
            }
            if(y> 0){
            //Agarra 5 (1-5)
            if(myarray[x][y] === 5 && myarray[x][y-1] === 1){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x][y-1] = 0;
                document.getElementById('c'+x+(y-1)).src = tablero;
                valoresBooleanos();
            }
            //Agarra 1 (5-1)
            if(myarray[x][y] === 1 && myarray[x][y-1] === 5){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x][y-1] = 0;
                document.getElementById('c'+x+(y-1)).src = tablero;
                valoresBooleanos();
            }
            }
        /*VERTICAL*/
        //Agarra 1 (1/5)
        if(x<3){
            if((myarray[x][y] === 1) && (myarray[x+1][y] === 5)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x+1][y] = 0;
                document.getElementById('c'+(x+1)+y).src = tablero;
                valoresBooleanos();
            }
        }
        //Agarra 5 (5/1)
        if((myarray[x][y] === 5) && (myarray[x+1][y] === 1)){
            myarray[x][y] = 'W'; // G de Grises
            document.getElementById(id).src = well;
            myarray[x+1][y] = 0;
            document.getElementById('c'+(x+1)+y).src = tablero;
            valoresBooleanos();
        }
        
        if(x> 0){
            //Agarra 5 (1/5)
            if((myarray[x][y] === 5) && (myarray[x-1][y] === 1)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x-1][y] = 0;
                document.getElementById('c'+(x-1)+y).src = tablero;
                valoresBooleanos();
            }
            //Agarra 1 (5/1)
            if((myarray[x][y] === 1) && (myarray[x-1][y] === 5)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x-1][y] = 0;
                document.getElementById('c'+(x-1)+y).src = tablero;
                valoresBooleanos();

            }
        }
        if(x<3){
            if((myarray[x][y] === 1) && (myarray[x+1][y] === 1)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x-1][y] = 0;
                document.getElementById('c'+(x-1)+y).src = tablero;
                valoresBooleanos();
            }
            if((myarray[x][y] === 5) && (myarray[x+1][y] === 5)){
                myarray[x][y] = 'W'; // G de Grises
                document.getElementById(id).src = well;
                myarray[x-1][y] = 0;
                document.getElementById('c'+(x-1)+y).src = tablero;
                valoresBooleanos();
    
            }
        }
        if(validarEdificio === false){
            alert('Elige un campo adecuado');
        }

        }//Termina if Well
        if(edificioParaC === 1){//TERMINADO
            //h1 y v2
            if(x < 3){
                if(y > 0){
                    if((myarray[x+1][y] === 4 && myarray[x+1][y-1] === 3 && myarray[x][y] === 2) || (myarray[x+1][y] === 4 && myarray[x+1][y-1] === 2 && myarray[x][y] === 3)){//Si agarra
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x+1][y] = 0;
                        myarray[x+1][y-1] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                    
                }
            }
            if(x > 0){
                if(x < 3){
                    if((myarray[x][y+1] === 4 && myarray[x-1][y+1] === 2 && myarray[x][y] === 3) || (myarray[x][y+1] === 4 && myarray[x-1][y+1] === 3 && myarray[x][y] === 2)){//Si agarra
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x][y+1] = 0;
                        myarray[x-1][y+1] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(y > 0){
                    if((myarray[x-1][y] === 2 && myarray[x][y-1] === 3 && myarray[x][y] === 4) || (myarray[x-1][y] === 3 && myarray[x][y-1] === 2 && myarray[x][y] === 4)){//Si agarra
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x-1][y] = 0;
                        myarray[x][y-1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            //H2 y V4
            if(x < 3){
                if(y > 0){
                    if((myarray[x][y] === 4 && myarray[x][y-1] === 3 && myarray[x+1][y] === 2) || (myarray[x][y] === 4 && myarray[x][y-1] === 2 && myarray[x+1][y] === 3)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x][y-1] = 0;
                        myarray[x+1][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(y < 3){
                    if((myarray[x][y] === 3 && myarray[x][y+1] === 4 && myarray[x+1][y+1] === 2) || (myarray[x][y] === 2 && myarray[x][y+1] === 4 && myarray[x+1][y+1] === 3)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x][y+1] = 0;
                        myarray[x+1][y+1] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            if(x > 0){
                if(y > 0){
                    if((myarray[x][y] === 2 && myarray[x-1][y] === 4 && myarray[x-1][y-1] === 3) || (myarray[x][y] === 3 && myarray[x-1][y] === 4 && myarray[x-1][y-1] === 2)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x-1][y] = 0;
                        myarray[x-1][y-1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            //H3 y V3
            if(x < 3){
                if(y < 3){
                    if((myarray[x][y] === 4 && myarray[x][y+1] === 3 && myarray[x+1][y] === 2) || (myarray[x][y] === 4 && myarray[x][y+1] === 2 && myarray[x+1][y] === 3)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x][y+1] = 0;
                        myarray[x+1][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(y > 0){
                    if((myarray[x][y] === 3 && myarray[x][y-1] === 4 && myarray[x+1][y-1] === 2) || (myarray[x][y] === 2 && myarray[x][y-1] === 4 && myarray[x+1][y-1] === 3)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x][y-1] = 0;
                        myarray[x+1][y-1] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            if(x > 0){
                if(y < 3){
                    if((myarray[x][y] === 2 && myarray[x-1][y] === 4 && myarray[x-1][y+1] === 3) || (myarray[x][y] === 3 && myarray[x-1][y] === 4 && myarray[x-1][y+1] === 2)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x-1][y] = 0;
                        myarray[x-1][y+1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }    
                }
            }
            // H4 y V1
            if(x < 3){
                if(y < 3){
                    if((myarray[x][y] === 2 && myarray[x+1][y] === 4 && myarray[x+1][y+1] === 3) || (myarray[x][y] === 3 && myarray[x+1][y] === 4 && myarray[x+1][y+1] === 2)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x+1][y] = 0;
                        myarray[x+1][y+1] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }    
                }
            }
            if(x > 0){
                if(y < 3){
                    if((myarray[x][y] === 4 && myarray[x-1][y] === 2 && myarray[x][y+1] === 3) || (myarray[x][y] === 4 && myarray[x-1][y] === 3 && myarray[x][y+1] === 2)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x-1][y] = 0;
                        myarray[x][y+1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }    
                }
                if(y > 0){
                    if((myarray[x][y] === 3 && myarray[x][y-1] === 4 && myarray[x-1][y-1] === 2) || (myarray[x][y] === 2 && myarray[x][y-1] === 4 && myarray[x-1][y-1] === 3)){
                        myarray[x][y] = 'C'; 
                        document.getElementById(id).src = cottage;
                        myarray[x][y-1] = 0;
                        myarray[x-1][y-1] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }    
                }
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }

        }//Termina if Cottage
        if(edificioParaC === 3){//TERMINADO
        /*HORIZONTALES*/
            //Agarra 2 arriba izq (2-2 / 1-1) Ya quedo
            if((myarray[x][y] === 2) && (myarray[x][(y+1)] === 2)){
                if(x<3){
                    if((myarray[(x+1)][y] === 1) && (myarray[(x+1)][(y+1)] === 1)){ //H1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y+1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 1) && (myarray[(x-1)][(y+1)] ===1)){
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y+1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            //Agarra 2 arriba der (2-2 / 1-1) Ya quedo
            if((myarray[x][y] === 2) && (myarray[x][y-1] === 2)){
                if(x<3){
                    if((myarray[(x+1)][y] === 1) && (myarray[(x+1)][(y-1)] === 1)){ //H1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y-1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 1) && (myarray[(x-1)][(y-1)] ===1)){
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y-1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            //Agarra 1 arriba izq (2-2 / 1-1) Ya quedo
            if((myarray[x][y] === 1) && (myarray[x][(y+1)] === 1)){
                if(x<3){
                    if((myarray[(x+1)][y] === 2) && (myarray[(x+1)][(y+1)] === 2)){ //H1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y+1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 2) && (myarray[(x-1)][(y+1)] ===2)){
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y+1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
            //Agarra 1 arriba der (2-2 / 1-1) Ya quedo
            if((myarray[x][y] === 1) && (myarray[x][y-1] === 1)){
                if(x<3){
                    if((myarray[(x+1)][y] === 2) && (myarray[(x+1)][(y-1)] === 2)){ //H1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y-1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 2) && (myarray[(x-1)][(y-1)] ===2)){
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[x][y-1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                    }
                }
            }
        /*VERTICALES*/
            //Agarra 2 arriba (2/2 - 1/1)
            if((myarray[x][y] === 2) && (myarray[(x+1)][y] === 2)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 1) && (myarray[(x+1)][(y+1)] === 1)){ //V1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 1) && (myarray[(x+1)][(y-1)] === 1)){ //V2
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                } 
            }
            //Agarra 2 abajo (2/2 - 1/1)
            if((myarray[x][y] === 2) && (myarray[(x-1)][y] === 2)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 1) && (myarray[(x-1)][(y+1)] === 1)){ //V1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x-1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 1) && (myarray[(x-1)][(y-1)] === 1)){ //V2
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x-1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 1 arriba (2/2 - 1/1)
            if((myarray[x][y] === 1) && (myarray[(x+1)][y] === 1)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 2) && (myarray[(x+1)][(y+1)] === 2)){ //V1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 2) && (myarray[(x+1)][(y-1)] === 2)){ //V2
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                } 
            }
            //Agarra 1 abajo (1/1 - 2/2)
            if((myarray[x][y] === 1) && (myarray[(x-1)][y] === 1)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 2) && (myarray[(x-1)][(y+1)] === 2)){ //V1
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x-1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 2) && (myarray[(x-1)][(y-1)] === 2)){ //V2
                        myarray[x][y] = 'F'; // Farm
                        document.getElementById(id).src = farm;
                        myarray[(x-1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }
        }//Termina if Farm
        if(edificioParaC === 4){//TERMINADO 
        /*HORIZONTALES*/
        //H1 - H2    
            //Agarra en 5 orilla (3-5-5) Ya quedo
            if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 5) && (myarray[x][(y-2)] === 3)){
                if(x>0){
                    if ((myarray[(x-1)][y] === 4)){//H1
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[x][(y-1)] = 0;
                        myarray[x][(y-2)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y-2)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x<3){
                    if((myarray[(x+1)][y] === 4)){//H2
                        myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[x][(y-1)] = 0;
                            myarray[x][(y-2)] = 0;
                            myarray[(x+1)][y] = 0;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+x+(y-2)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            valoresBooleanos();
                    }
                }
            }
            //Agarra 5 medio (3-5-5) Ya quedo
            if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 5) && (myarray[x][(y-1)] === 3)){
                if(x>0){
                    if ((myarray[(x-1)][(y+1)] === 4)){//H1
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        //console.log(' 5 en medio H1 (3-5-5)')
                        myarray[x][(y+1)]  = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x<3){
                    if((myarray[(x+1)][(y+1)] === 4)){//H2
                        myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            //console.log(' 5 en medio H2 (3-5-5)')
                            myarray[x][(y+1)]  = 0;
                            myarray[x][(y-1)] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                    }
                }
            }
            //Agarra 3 (3-5-5) Ya quedo
            if((myarray[x][y] === 3) && (myarray[x][(y+1)] === 5) && (myarray[x][(y+2)] === 5)){
                if(x>0){
                    if ((myarray[(x-1)][(y+2)] === 4)){//H1
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        //console.log(' 5 en medio H1 (3-5-5)')
                        myarray[x][(y+1)]  = 0;
                        myarray[x][(y+2)]  = 0;
                        myarray[(x-1)][(y+2)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y+2)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+2)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x<3){
                    if((myarray[(x+1)][(y+2)] === 4)){//H2
                        myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            //console.log(' 5 en medio H2 (3-5-5)')
                            myarray[x][(y+1)]  = 0;
                            myarray[x][(y+2)]  = 0;
                            myarray[(x+1)][(y+2)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y+2)).src = tablero;
                            document.getElementById('c'+(x+1)+(y+2)).src = tablero;
                            valoresBooleanos();
                    }
                }
            }
            //Agarra en 4 (3-5-5) Ya quedo
            if((myarray[x][y] === 4)){
                if(x<3){
                    if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y-1)] === 5) && (myarray[(x+1)][(y-2)] === 3)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        myarray[(x+1)][(y-2)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-2)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if((x>0)){
                    if((myarray[(x-1)][y] === 5) && (myarray[(x-1)][y-1] === 5) && (myarray[(x-1)][(y-2)] === 3)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][y-1] = 0;
                        myarray[(x-1)][(y-2)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-2)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }

        //H3 - H4
            //Agarra 5 orilla (5-5-3) Ya quedo
            if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 5) && (myarray[x][(y+2)] === 3)){
                if(x>0){
                    if ((myarray[(x-1)][y] === 4)){//H3
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[x][(y+1)] = 0;
                        myarray[x][(y+2)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y+2)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x<3){
                    if((myarray[(x+1)][y] === 4)){//H4
                        myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[x][(y+1)] = 0;
                            myarray[x][(y+2)] = 0;
                            myarray[(x+1)][y] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y+2)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            valoresBooleanos();
                    }
                }
            }
            //Agarra 5 medio (5-5-3) Ya quedo
            if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 5) && (myarray[x][(y+1)] === 3)){
                if((x>0) && (y>0)){
                    if ((myarray[(x-1)][(y-1)] === 4)){//H3
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[x][(y-1)] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if((myarray[(x+1)][(y-1)] === 4)){//H4
                    myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        console.log('5 en medio H4');
                        myarray[x][(y-1)] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                }
            }
            //Agarra 3 (5-5-3) Ya quedo
            if((myarray[x][y] === 3) && (myarray[x][(y-1)] === 5) && (myarray[x][(y-2)] === 5)){
                if ((myarray[(x-1)][(y-2)] === 4)){//H3
                    myarray[x][y] = 'A';
                    document.getElementById(id).src = abbey;
                    myarray[x][(y-1)] = 0;
                    myarray[x][(y-2)] = 0;
                    myarray[(x-1)][(y-2)] = 0;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+x+(y-2)).src = tablero;
                    document.getElementById('c'+(x-1)+(y-2)).src = tablero;
                    valoresBooleanos();
                }
                else if((myarray[(x+1)][(y-2)] === 4)){//H4
                    myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        console.log('5 en medio H4');
                        myarray[x][(y-1)] = 0;
                        myarray[x][(y-2)] = 0;
                        myarray[(x+1)][(y-2)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y-2)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-2)).src = tablero;
                        valoresBooleanos();
                }
            }
            //Agarra 4 (5-5-3) Ya quedo
            if((myarray[x][y] === 4)){
                if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y+1)] === 5) && (myarray[(x+1)][(y+2)] === 3)){
                    myarray[x][y] = 'A';
                    document.getElementById(id).src = abbey;
                    myarray[(x+1)][y] = 0;
                    myarray[(x+1)][(y+1)] = 0;
                    myarray[(x+1)][(y+2)] = 0;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y+2)).src = tablero;
                    valoresBooleanos();
                }
                else if(x>0){
                    if((myarray[(x-1)][y] === 5) && (myarray[(x-1)][(y+1)] === 5) && (myarray[(x-1)][(y+2)] === 3)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        myarray[(x-1)][(y+2)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+2)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }


        /*VERTICALES*/
        //V1 - V2
            //Agarra 5 arriba (5/5/3) Ya quedo
            if(x<2){
                if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 5) && (myarray[(x+2)][y] === 3)){
                    if(y < 3){
                        if((myarray[x][(y+1)] === 4)){
                            myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+2)][y] = 0;
                            myarray[x][(y+1)] = 0;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+2)+y).src = tablero;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 4)){
                            myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+2)][y] = 0;
                            myarray[x][(y-1)] = 0;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+2)+y).src = tablero;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 5 medio (5/5/3) Ya quedo
            if(x<3){
                if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 5) && (myarray[(x+1)][y] === 3)){
                    if(x>0){
                        if(y < 3){
                            if((myarray[(x-1)][(y+1)] === 4)){
                                myarray[x][y] = 'A';
                                document.getElementById(id).src = abbey;
                                myarray[(x-1)][y] = 0;
                                myarray[(x+1)][y] = 0;
                                myarray[(x-1)][(y+1)] = 0;
                                document.getElementById('c'+(x-1)+y).src = tablero;
                                document.getElementById('c'+(x+1)+y).src = tablero;
                                document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                                valoresBooleanos();
                            }
                        }
                        if(y > 0){
                            if((myarray[(x-1)][(y-1)] === 4)){
                                myarray[x][y] = 'A';
                                document.getElementById(id).src = abbey;
                                myarray[(x-1)][y] = 0;
                                myarray[(x+1)][y] = 0;
                                myarray[(x-1)][(y-1)] = 0;
                                document.getElementById('c'+(x-1)+y).src = tablero;
                                document.getElementById('c'+(x+1)+y).src = tablero;
                                document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                                valoresBooleanos();
                            }
                        }
                    }
                }
            }
            //Agarra 3 (5/5/3) Ya quedo
            if(x>1){
                if((myarray[x][y] === 3) && (myarray[(x-1)][y] === 5) && (myarray[(x-2)][y] === 5)){
                    if(y < 3){
                        if((myarray[(x-2)][(y+1)] === 4)){
                            myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[(x-2)][(y+1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+(x-2)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[(x-2)][(y-1)] === 4)){
                            myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[(x-2)][(y-1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+(x-2)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 4 (5/5/3) Ya quedo
            if((myarray[x][y] === 4)){
                if((myarray[x][(y-1)] === 5) && (myarray[(x+1)][(y-1)] === 5) && (myarray[(x+2)][(y-1)] === 3)){
                    myarray[x][y] = 'A';
                    document.getElementById(id).src = abbey;
                    myarray[x][(y-1)] = 0;
                    myarray[(x+1)][(y-1)] = 0;
                    myarray[(x+2)][(y-1)] = 0;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                    document.getElementById('c'+(x+2)+(y-1)).src = tablero;
                    valoresBooleanos();
                }
                if((myarray[x][(y+1)] === 5) && (myarray[(x+1)][(y+1)] === 5) && (myarray[(x+2)][(y+1)] === 3)){
                    myarray[x][y] = 'A';
                    document.getElementById(id).src = abbey;
                    myarray[x][(y+1)] = 0;
                    myarray[(x+1)][(y+1)] = 0;
                    myarray[(x+2)][(y+1)] = 0;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                    document.getElementById('c'+(x+2)+(y+1)).src = tablero;
                    valoresBooleanos();
                }
            }

        //V3 - V4
            //Agarra 5 abajo (3/5/5) Ya quedo
            if(x>1){
                if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 5) && (myarray[(x-2)][y] === 3)){
                    if(y < 3){
                        if((myarray[x][(y+1)] === 4)){
                            myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[x][(y+1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 4)){
                            myarray[x][y] = 'A';
                            document.getElementById(id).src = abbey;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[x][(y-1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    
                }
            }
            //Agarra 5 medio (3/5/5) Ya quedo
            if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 5) && (myarray[(x-1)][y] === 3)){
                if(y < 3){
                    if((myarray[(x+1)][(y+1)] === 4)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x+1)][y] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[(x+1)][(y-1)] === 4)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x+1)][y] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x+1)][(y-1)]
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                
            }
            //Agarra 3 (3/5/5) Ya quedo
            if((myarray[x][y] === 3) && (myarray[(x+1)][y] === 5) && (myarray[(x+2)][y] === 5)){
                if(y < 3){
                    if((myarray[(x+2)][(y+1)] === 4)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+2)][y] = 0;
                        myarray[(x+2)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+2)+y).src = tablero;
                        document.getElementById('c'+(x+2)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[(x+2)][(y-1)] === 4)){
                        myarray[x][y] = 'A';
                        document.getElementById(id).src = abbey;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+2)][y] = 0;
                        myarray[(x+2)][(y-1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+2)+y).src = tablero;
                        document.getElementById('c'+(x+2)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                
            }
            //Agarra 4 (3/5/5) Ya quedo
            if((myarray[x][y] === 4)){
                if((myarray[x][(y-1)] === 5) && (myarray[(x-1)][(y-1)] === 5) && (myarray[(x-2)][(y-1)] === 3)){
                    myarray[x][y] = 'A';
                    document.getElementById(id).src = abbey;
                    myarray[x][(y-1)] = 0;
                    myarray[(x-1)][(y-1)] = 0;
                    myarray[(x-2)][(y-1)] = 0;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                    document.getElementById('c'+(x-2)+(y-1)).src = tablero;
                    valoresBooleanos();
                }
                if((myarray[x][(y+1)] === 5) && (myarray[(x-1)][(y+1)] === 5) && (myarray[(x-2)][(y+1)] === 3)){
                    myarray[x][y] = 'A';
                    document.getElementById(id).src = abbey;
                    myarray[x][(y+1)] = 0;
                    myarray[(x-1)][(y+1)] = 0;
                    myarray[(x-2)][(y+1)] = 0;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                    document.getElementById('c'+(x-2)+(y+1)).src = tablero;
                    valoresBooleanos();
                }
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }
        }//Termina if Abbey
        if(edificioParaC === 5){//TERMINADO
            if(y < 2){
                if((myarray[x][y] === 3 && myarray[x][y+1] === 3 && myarray[x][y+2] === 4) || (myarray[x][y] === 4 && myarray[x][y+1] === 3 && myarray[x][y+2] === 3)){
                    myarray[x][y] = 'T';
                    document.getElementById(id).src = tavern;
                    myarray[x][y+1] = 0;
                    myarray[x][(y+2)] = 0;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+x+(y+2)).src = tablero;
                    valoresBooleanos();  
                }
            }
            if(y > 0 && y<3){
                if((myarray[x][y] === 3 && myarray[x][y-1] === 3 && myarray[x][y+1] === 4) || (myarray[x][y] === 3 && myarray[x][y-1] === 4 && myarray[x][y+1] === 3)){
                    myarray[x][y] = 'T';
                    document.getElementById(id).src = tavern;
                    myarray[x][y-1] = 0;
                    myarray[x][(y+1)] = 0;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    valoresBooleanos();  
                }
            }
            if(y > 1){
                if((myarray[x][y] === 4 && myarray[x][y-1] === 3 && myarray[x][y-2] === 3) || (myarray[x][y] === 3 && myarray[x][y-1] === 3 && myarray[x][y-2] === 4)){
                    myarray[x][y] = 'T';
                    document.getElementById(id).src = tavern;
                    myarray[x][y-1] = 0;
                    myarray[x][(y-2)] = 0;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+x+(y-2)).src = tablero;
                    valoresBooleanos();  
                } 
            }
            //Vertical
            if(x < 2){
                if((myarray[x][y] === 3 && myarray[x+1][y] === 3 && myarray[x+2][y] === 4) || (myarray[x][y] === 4 && myarray[x+1][y] === 3 && myarray[x+2][y] === 3)){
                    myarray[x][y] = 'T';
                    document.getElementById(id).src = tavern;
                    myarray[x+1][y] = 0;
                    myarray[x+2][y] = 0;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    document.getElementById('c'+(x+2)+y).src = tablero;
                    valoresBooleanos();  
                }
            }
            if(x > 0 && x<3){
                if((myarray[x][y] === 3 && myarray[x-1][y] === 3 && myarray[x+1][y] === 4) || (myarray[x][y] === 3 && myarray[x-1][y] === 4 && myarray[x+1][y] === 3)){
                    myarray[x][y] = 'T';
                    document.getElementById(id).src = tavern;
                    myarray[x-1][y] = 0;
                    myarray[x+1][y] = 0;
                    document.getElementById('c'+(x-1)+y).src = tablero;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    valoresBooleanos();  
                }
            }
            if(x > 1){
                if((myarray[x][y] === 4 && myarray[x-1][y] === 3 && myarray[x-2][y] === 3) || (myarray[x][y] === 3 && myarray[x-1][y] === 3 && myarray[x-2][y] === 4)){
                    myarray[x][y] = 'T';
                    document.getElementById(id).src = tavern;
                    myarray[x-1][y] = 0;
                    myarray[x-2][y] = 0;
                    document.getElementById('c'+(x-1)+y).src = tablero;
                    document.getElementById('c'+(x-2)+y).src = tablero;
                    valoresBooleanos();  
                } 
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }
        }//Termina if Tavern
        if(edificioParaC === 6){//TERMINADO
        /*HORIZONTALES*/
            //H1 - H2
            //Agarra 3 izq (3-4-3) Ya quedo
            if(y < 2){
                if((myarray[x][y] === 3) && (myarray[x][(y+1)] === 4) && (myarray[x][(y+2)] === 3)){
                    //alert("3-4-3");
                    if(x>0){
                        if((myarray[(x-1)][(y+1)] === 2)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[x][(y+1)] = 0;
                            myarray[x][(y+2)] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y+2)).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][(y+1)] === 2)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[x][(y+1)] = 0;
                            myarray[x][(y+2)] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y+2)).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    
                }
            }
            //Agarra 3 der (3-4-3) Ya quedo
            if((myarray[x][y] === 3) && (myarray[x][(y-1)] === 4) && (myarray[x][(y-2)] === 3)){
                if(x>0){
                    if((myarray[(x-1)][(y-1)] === 2)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[x][(y-1)] = 0;
                        myarray[x][(y-2)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y-2)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x < 3){
                    if((myarray[(x+1)][(y-1)] === 2)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[x][(y-1)] = 0;
                        myarray[x][(y-2)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y-2)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                
            }
            //Agarra 4 medio (3-4-3) Ya quedo
            if((myarray[x][y] === 4) && (myarray[x][(y+1)] === 3) && (myarray[x][(y-1)] === 3)){
                if(x>0){
                    if((myarray[(x-1)][y] === 2)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[x][(y+1)] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x < 3){
                    if((myarray[(x+1)][y] === 2)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[x][(y+1)] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
                
            }
            //Agarra 2 (3-4-3) Ya quedo
            if((myarray[x][y] === 2)){
                if(y>0){
                    if(x<3){
                        if((myarray[(x+1)][(y-1)] === 3) && (myarray[(x+1)][y] === 4) && (myarray[(x+1)][(y+1)] === 3)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[(x+1)][(y-1)] = 0;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if((myarray[(x-1)][(y-1)] === 3) && (myarray[(x-1)][y] === 4) && (myarray[(x-1)][(y+1)] === 3)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[(x-1)][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
        /*VERTICALES*/
            //V1 - V2
            //Agarra 3 arriba (3/4/3) Ya quedo
            if(x < 2){
                if((myarray[x][y] === 3) && (myarray[(x+1)][y] === 4) && (myarray[(x+2)][y] === 3)){
                    if(y < 3){
                        if((myarray[(x+1)][(y+1)] === 2)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[(x+1)][y]= 0;
                            myarray[(x+2)][y] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+2)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[(x+1)][(y-1)] === 2)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[(x+1)][y]= 0;
                            myarray[(x+2)][y] = 0;
                            myarray[(x+1)][(y-1)] = 0;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+2)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 3 abajo (3/4/3) Ya quedo
            if(x > 1){
                if((myarray[x][y] === 3) && (myarray[(x-1)][y] === 4) && (myarray[(x-2)][y] === 3)){
                    if(y < 3){
                        if((myarray[(x-1)][(y+1)] === 2)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[(x-1)][(y-1)] === 2)){
                            myarray[x][y] = 'B';
                            document.getElementById(id).src = bakery;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 4 medio (3/4/3) Ya quedo
            if((myarray[x][y] === 4) && (myarray[(x-1)][y] === 3) && (myarray[(x+1)][y] === 3)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 2)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[(x-1)][y] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 2)){
                        myarray[x][y] = 'B';
                        document.getElementById(id).src = bakery;
                        myarray[(x-1)][y] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 2 (3/4/3) Ya quedo
            if((myarray[x][y] === 2)){
                if((myarray[(x-1)][(y-1)] === 3) && (myarray[x][(y-1)] === 4) && (myarray[(x+1)][(y-1)] === 3)){
                    myarray[x][y] = 'B';
                    document.getElementById(id).src = bakery;
                    myarray[(x-1)][(y-1)] = 0;
                    myarray[x][(y-1)] = 0;
                    myarray[(x+1)][(y-1)] = 0;
                    document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                    valoresBooleanos();
                }
                if((myarray[(x-1)][(y+1)] === 3) && (myarray[x][(y+1)] === 4) && (myarray[(x+1)][(y+1)] === 3)){
                    myarray[x][y] = 'B';
                    document.getElementById(id).src = bakery;
                    myarray[(x-1)][(y+1)] = 0;
                    myarray[x][(y+1)] = 0;
                    myarray[(x+1)][(y+1)] = 0;
                    document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                    valoresBooleanos();
                }
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }
        }//Termina if Bakery
        if(edificioParaC === 7){//TERMINADO
        /*HORIZONTALES*/
            //H1 -H2
            //Agarra 5 abajo (5-1-3) Ya quedo
            if(y < 2){
                if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 1) && (myarray[x][(y+2)] === 3)){
                    if(x>0){
                        if((myarray[x-1][y] === 5) && (myarray[(x-1)][(y+1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y+1)] = 0;
                            myarray[x][(y+2)] = 0;
                            myarray[x-1][y] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y+2)).src = tablero;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y+1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y+1)] = 0;
                            myarray[x][(y+2)] = 0;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+x+(y+2)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 1 abajo (5-1-3) Ya quedo
            if((myarray[x][y] === 1) && (myarray[x][(y-1)] === 5) && (myarray[x][(y+1)] === 3)){
                if(x>0){
                    if((myarray[x-1][(y-1)] === 5) && (myarray[(x-1)][y] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y-1)]  = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[x-1][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x < 3){
                    if((myarray[(x+1)][(y-1)] === 5) && (myarray[(x+1)][y] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y-1)]  = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[x+1][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 3 (5-1-3) Ya quedo
            if((myarray[x][y] === 3) && (myarray[x][(y-1)] === 1) && (myarray[x][(y-2)] === 5)){
                if(x>0){
                    if((myarray[x-1][(y-2)] === 5) && (myarray[(x-1)][(y-1)] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y-1)]  = 0;
                        myarray[x][(y-2)] = 0;
                        myarray[(x-1)][(y-2)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y-2)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-2)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x < 3){
                    if((myarray[(x+1)][(y-2)] === 5) && (myarray[(x+1)][(y-1)] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y-1)]  = 0;
                        myarray[x][(y-2)] = 0;
                        myarray[(x+1)][(y-2)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+x+(y-2)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-2)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 5 arriba (5-1-3) Ya quedo
            if((myarray[x][y] === 5) && (myarray[x][(y+1)] === 1)){
                if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y+1)] === 1) && (myarray[(x+1)][(y+2)] == 3)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[x][(y+1)] = 0;
                    myarray[(x+1)][y] = 0;
                    myarray[(x+1)][(y+1)] = 0;
                    myarray[(x+1)][(y+2)] = 0;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y+2)).src = tablero;
                    valoresBooleanos();
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 5) && (myarray[(x-1)][(y+1)] === 1) && (myarray[(x-1)][(y+2)] == 3)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        myarray[(x-1)][(y+2)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+2)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 1 arriba (5-1-3) Ya quedo
            if(y>0){
                if((myarray[x][y] === 1) && (myarray[x][(y-1)] === 5)){
                    if(x<3){
                        if((myarray[(x+1)][(y-1)] === 5) && (myarray[(x+1)][y] === 1) && (myarray[(x+1)][(y+1)] == 3)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y-1)] = 0;
                            myarray[(x+1)][(y-1)] = 0;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(x>0){
                        if((myarray[(x-1)][(y-1)] === 5) && (myarray[(x-1)][y] === 1) && (myarray[(x-1)][(y+1)] == 3)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y-1)] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }

            //H3 -H4
            //Agarra 5 abajo (3-1-5) Ya quedo 
            if(y > 1){
                if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 1) && (myarray[x][(y-2)] === 3)){
                    if(x > 0){
                        if((myarray[(x-1)][y] === 5) && (myarray[(x-1)][(y-1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y-1)]  = 0;
                            myarray[x][(y-2)] = 0;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+x+(y-2)).src = tablero;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(x < 3){
                        if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y-1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y-1)]  = 0;
                            myarray[x][(y-2)] = 0;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+1)][(y-1)] = 0;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+x+(y-2)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 1 abajo (3-1-5) Ya quedo
            if((myarray[x][y] === 1) && (myarray[x][(y+1)] === 5) && (myarray[x][(y-1)] === 3)){
                if(x>0){
                    if((myarray[x-1][(y+1)] === 5) && (myarray[(x-1)][y] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y+1)]  = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[x-1][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x < 3){
                    if((myarray[(x+1)][(y+1)] === 5) && (myarray[(x+1)][y] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y+1)]  = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[x+1][(y+1)] = 0;
                        myarray[(x+1)][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 3 (3-1-5) Ya quedo
            if((myarray[x][y] === 3) && (myarray[x][(y+1)] === 1) && (myarray[x][(y+2)] === 5)){
                if(x>0){
                    if((myarray[x-1][(y+2)] === 5) && (myarray[(x-1)][(y+1)] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y+1)]  = 0;
                        myarray[x][(y+2)] = 0;
                        myarray[(x-1)][(y+2)] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y+2)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+2)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(x < 3){
                    if((myarray[(x+1)][(y+2)] === 5) && (myarray[(x+1)][(y+1)] === 1)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y+1)]  = 0;
                        myarray[x][(y+2)] = 0;
                        myarray[(x+1)][(y+2)] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+x+(y+2)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+2)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 5 arriba (3-1-5) Ya quedo
            if((myarray[x][y] === 5) && (myarray[x][(y-1)] === 1)){
                if((myarray[(x+1)][y] === 5) && (myarray[(x+1)][(y-1)] === 1) && (myarray[(x+1)][(y-2)] == 3)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[x][(y-1)] = 0;
                    myarray[(x+1)][y] = 0;
                    myarray[(x+1)][(y-1)] = 0;
                    myarray[(x+1)][(y-2)] = 0;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y-2)).src = tablero;
                    valoresBooleanos();
                }
                if(x>0){
                    if((myarray[(x-1)][y] === 5) && (myarray[(x-1)][(y-1)] === 1) && (myarray[(x-1)][(y-2)] == 3)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        myarray[(x-1)][(y-2)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-2)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 1 arriba (3-1-5) Ya quedo
            if(y>0){
                if((myarray[x][y] === 1) && (myarray[x][(y+1)] === 5)){
                    if(x<3){
                        if((myarray[(x+1)][(y+1)] === 5) && (myarray[(x+1)][y] === 1) && (myarray[(x+1)][(y-1)] == 3)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y+1)] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+1)][(y-1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(x>0){
                        if((myarray[(x-1)][(y+1)] === 5) && (myarray[(x-1)][y] === 1) && (myarray[(x-1)][(y-1)] == 3)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[x][(y+1)] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }

        /*VERTICALES*/
           //V1 - V2
           //Agarra 5 ar-izq (5/1/3) Ya quedo
           if(x < 2){
                if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 1) && (myarray[(x+2)][y] === 3)){
                    //alert("5/1/3");
                    if(y < 3){
                        if((myarray[x][(y+1)] === 5) && (myarray[(x+1)][(y+1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+2)][y] = 0;
                            myarray[x][(y+1)] = 0;
                            myarray[(x+1)][(y+1)] = 0;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+2)+y).src = tablero;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 5) && (myarray[(x+1)][(y-1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[(x+1)][y] = 0;
                            myarray[(x+2)][y] = 0;
                            myarray[x][(y-1)] = 0;
                            myarray[(x+1)][(y-1)] = 0;
                            document.getElementById('c'+(x+1)+y).src = tablero;
                            document.getElementById('c'+(x+2)+y).src = tablero;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 1 medio-izq (5/1/3) Ya quedo
            if((myarray[x][y] === 1) && (myarray[(x-1)][y] === 5) && (myarray[(x+1)][y] === 3)){
                if(y < 3){
                    if((myarray[x][(y+1)] === 1) && (myarray[(x-1)][(y+1)] === 5)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x-1)][y] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 1) && (myarray[(x-1)][(y-1)] === 5)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x-1)][y] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 3 fin (5/1/3) Ya quedo
            if (x>1){
                if((myarray[x][y] === 3) && (myarray[(x-1)][y] === 1) && (myarray[(x-2)][y] === 5)){
                    if(y < 3){
                        if((myarray[(x-1)][(y+1)] === 1) && (myarray[(x-2)][(y+1)] === 5)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            myarray[(x-2)][(y+1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            document.getElementById('c'+(x-2)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[(x-1)][(y-1)] === 1) && (myarray[(x-2)][(y-1)] === 5)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            myarray[(x-2)][(y-1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            document.getElementById('c'+(x-2)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 5 ar-der (5/1/3) Ya quedo
            if(x<3){
                if((myarray[x][y] === 5) && (myarray[(x+1)][y] === 1)){
                    if((myarray[x][(y-1)] === 5) && (myarray[(x+1)][(y-1)] === 1) && (myarray[(x+2)][(y-1)] === 3)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        myarray[(x+2)][(y-1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x+2)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                    if((myarray[x][(y+1)] === 5) && (myarray[(x+1)][(y+1)] === 1) && (myarray[(x+2)][(y+1)] === 3)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x+1)][y] = 0;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        myarray[(x+2)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        document.getElementById('c'+(x+2)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 1 ab-der (5/1/3) Ya quedo
            if((myarray[x][y] === 1) && (myarray[(x-1)][y] === 5)){
                if((myarray[(x-1)][(y-1)] === 5) && (myarray[x][(y-1)] === 1) && (myarray[(x+1)][(y-1)] === 3)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[(x-1)][y] = 0;
                    myarray[(x-1)][(y-1)] = 0;
                    myarray[x][(y-1)] = 0;
                    myarray[(x+1)][(y-1)] = 0;
                    document.getElementById('c'+(x-1)+y).src = tablero;
                    document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                    valoresBooleanos();
                }
                if((myarray[(x-1)][(y+1)] === 5) && (myarray[x][(y+1)] === 1) && (myarray[(x+1)][(y+1)] === 3)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[(x-1)][y] = 0;
                    myarray[(x-1)][(y+1)] = 0;
                    myarray[x][(y+1)] = 0;
                    myarray[(x+1)][(y+1)] = 0;
                    document.getElementById('c'+(x-1)+y).src = tablero;
                    document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                    valoresBooleanos();
                }
            }

            //V3 - V4
            //Agarra 5 ab-izq (3/1/5) Ya quedo
            if(x > 1){  
                if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 1) && (myarray[(x-2)][y] === 3)){
                    //alert("3/1/5");
                    if(y < 3){
                        if((myarray[x][(y+1)] === 5) && (myarray[(x-1)][(y+1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[x][(y+1)] = 0;
                            myarray[(x-1)][(y+1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+x+(y+1)).src = tablero;
                            document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                    if(y > 0){
                        if((myarray[x][(y-1)] === 5) && (myarray[(x-1)][(y-1)] === 1)){
                            myarray[x][y] = 'TP';
                            document.getElementById(id).src = trading;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-2)][y] = 0;
                            myarray[x][(y-1)] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-2)+y).src = tablero;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            valoresBooleanos();
                        }
                    }
                }
            }
            //Agarra 1 medio-izq (3/1/5) Ya quedo
            if((myarray[x][y] === 1) && (myarray[(x+1)][y] === 5) && (myarray[(x-1)][y] === 3)){
                //alert("3/1/5");
                if(y < 3){
                    if((myarray[x][(y+1)] === 1) && (myarray[(x+1)][(y+1)] === 5)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x+1)][y] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[x][(y+1)]= 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[x][(y-1)] === 1) && (myarray[(x+1)][(y-1)] === 5)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x+1)][y] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 3 inicio (3/1/5)
            if((myarray[x][y] === 3) && (myarray[(x+1)][y] === 1) && (myarray[(x+2)][y] === 5)){
                if(y < 3){
                    if((myarray[(x+1)][(y+1)] === 1) && (myarray[(x+2)][(y+1)] === 5)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+2)][y] = 0;
                        myarray[(x+1)][(y+1)]= 0;
                        myarray[(x+2)][(y+1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+2)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        document.getElementById('c'+(x+2)+(y+1)).src = tablero;
                        valoresBooleanos();
                    }
                }
                if(y > 0){
                    if((myarray[(x+1)][(y-1)] === 1) && (myarray[(x+2)][(y-1)] === 5)){
                        myarray[x][y] = 'TP';
                        document.getElementById(id).src = trading;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+2)][y] = 0;
                        myarray[(x+1)][(y-1)]= 0;
                        myarray[(x+2)][(y-1)] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+2)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x+2)+(y-1)).src = tablero;
                        valoresBooleanos();
                    }
                }
            }
            //Agarra 5 ab-der (3/1/5)
            if((myarray[x][y] === 5) && (myarray[(x-1)][y] === 1)){
                if((myarray[x][(y-1)] === 5) && (myarray[(x-1)][(y-1)] === 1) && (myarray[(x-2)][(y-1)] === 3)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[(x-1)][y] = 0;
                    myarray[x][(y-1)] = 0;
                    myarray[(x-1)][(y-1)] = 0;
                    myarray[(x-2)][(y-1)] = 0;
                    document.getElementById('c'+(x-1)+y).src = tablero;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                    document.getElementById('c'+(x-2)+(y-1)).src = tablero;
                    valoresBooleanos();
                }
                if((myarray[x][(y+1)] === 5) && (myarray[(x-1)][(y+1)] === 1) && (myarray[(x-2)][(y+1)] === 3)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[(x-1)][y] = 0;
                    myarray[x][(y+1)] = 0;
                    myarray[(x-1)][(y+1)] = 0;
                    myarray[(x-2)][(y+1)] = 0;
                    document.getElementById('c'+(x-1)+y).src = tablero;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                    document.getElementById('c'+(x-2)+(y+1)).src = tablero;
                    valoresBooleanos();
                }
            }
            //Agarra 1 ab-der (3/1/5)
            if((myarray[x][y] === 1) && (myarray[(x+1)][y] === 5)){
                if((myarray[x][(y-1)] === 1) && (myarray[(x-1)][(y-1)] === 3) && (myarray[(x+1)][(y-1)] === 5)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[(x+1)][y] = 0;
                    myarray[x][(y-1)] = 0;
                    myarray[(x-1)][(y-1)] = 0;
                    myarray[(x+1)][(y-1)] = 0;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    document.getElementById('c'+x+(y-1)).src = tablero;
                    document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                    valoresBooleanos();
                }
                if((myarray[x][(y+1)] === 1) && (myarray[(x-1)][(y+1)] === 3) && (myarray[(x+1)][(y+1)] === 5)){
                    myarray[x][y] = 'TP';
                    document.getElementById(id).src = trading;
                    myarray[(x+1)][y] = 0;
                    myarray[x][(y+1)] = 0;
                    myarray[(x-1)][(y+1)] = 0;
                    myarray[(x+1)][(y+1)] = 0;
                    document.getElementById('c'+(x+1)+y).src = tablero;
                    document.getElementById('c'+x+(y+1)).src = tablero;
                    document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                    document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                    valoresBooleanos();
                }
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }
        }//Termina if Trading
        /*Monumentos*/
        if(edificio === 'Cathedral'){//TERMINADO
            if(x < 3){
                if(y > 0){
                    if((myarray[x+1][y] === 4 && myarray[x+1][y-1] === 5 && myarray[x][y] === 2) || (myarray[x+1][y] === 4 && myarray[x+1][y-1] === 2 && myarray[x][y] === 5)){//Si agarra
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x+1][y] = 0;
                        myarray[x+1][y-1] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                    
                }
            }
            if(x > 0){
                    if((myarray[x][y+1] === 4 && myarray[x-1][y+1] === 2 && myarray[x][y] === 5) || (myarray[x][y+1] === 4 && myarray[x-1][y+1] === 5 && myarray[x][y] === 2)){//Si agarra
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x][y+1] = 0;
                        myarray[x-1][y+1] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                if(y > 0){
                    if((myarray[x-1][y] === 2 && myarray[x][y-1] === 5 && myarray[x][y] === 4) || (myarray[x-1][y] === 5 && myarray[x][y-1] === 2 && myarray[x][y] === 4)){//Si agarra
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x-1][y] = 0;
                        myarray[x][y-1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                }
            }
            //H2 y V4
            if(x < 3){
                if(y > 0){
                    if((myarray[x][y] === 4 && myarray[x][y-1] === 5 && myarray[x+1][y] === 2) || (myarray[x][y] === 4 && myarray[x][y-1] === 2 && myarray[x+1][y] === 5)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x][y-1] = 0;
                        myarray[x+1][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                }
                if(y < 3){
                    if((myarray[x][y] === 5 && myarray[x][y+1] === 4 && myarray[x+1][y+1] === 2) || (myarray[x][y] === 2 && myarray[x][y+1] === 4 && myarray[x+1][y+1] === 5)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x][y+1] = 0;
                        myarray[x+1][y+1] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                }
            }
            if(x > 0){
                if(y > 0){
                    if((myarray[x][y] === 2 && myarray[x-1][y] === 4 && myarray[x-1][y-1] === 5) || (myarray[x][y] === 5 && myarray[x-1][y] === 4 && myarray[x-1][y-1] === 2)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x-1][y] = 0;
                        myarray[x-1][y-1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                }
            }
            //H3 y V3
            if(x < 3){
                if(y < 3){
                    if((myarray[x][y] === 4 && myarray[x][y+1] === 5 && myarray[x+1][y] === 2) || (myarray[x][y] === 4 && myarray[x][y+1] === 2 && myarray[x+1][y] === 5)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x][y+1] = 0;
                        myarray[x+1][y] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                }
                if(y > 0){
                    if((myarray[x][y] === 5 && myarray[x][y-1] === 4 && myarray[x+1][y-1] === 2) || (myarray[x][y] === 2 && myarray[x][y-1] === 4 && myarray[x+1][y-1] === 5)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x][y-1] = 0;
                        myarray[x+1][y-1] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }
                }
            }
            if(x > 0){
                if(y < 3){
                    if((myarray[x][y] === 2 && myarray[x-1][y] === 4 && myarray[x-1][y+1] === 5) || (myarray[x][y] === 5 && myarray[x-1][y] === 4 && myarray[x-1][y+1] === 2)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x-1][y] = 0;
                        myarray[x-1][y+1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }    
                }
            }
            // H4 y V1
            if(x < 3){
                if(y < 3){
                    if((myarray[x][y] === 2 && myarray[x+1][y] === 4 && myarray[x+1][y+1] === 5) || (myarray[x][y] === 5 && myarray[x+1][y] === 4 && myarray[x+1][y+1] === 2)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x+1][y] = 0;
                        myarray[x+1][y+1] = 0;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }    
                }
            }
            if(x > 0){
                if(y < 3){
                    if((myarray[x][y] === 4 && myarray[x-1][y] === 2 && myarray[x][y+1] === 5) || (myarray[x][y] === 4 && myarray[x-1][y] === 5 && myarray[x][y+1] === 2)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x-1][y] = 0;
                        myarray[x][y+1] = 0;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }    
                }
                if(y > 0){
                    if((myarray[x][y] === 5 && myarray[x][y-1] === 4 && myarray[x-1][y-1] === 2) || (myarray[x][y] === 2 && myarray[x][y-1] === 4 && myarray[x-1][y-1] === 5)){
                        myarray[x][y] = 'CC'; 
                        document.getElementById(id).src = cathedral;
                        myarray[x][y-1] = 0;
                        myarray[x-1][y-1] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true;
                    }    
                }
            }
            if(validarEdificio === false){
                alert('Elige un campo adecuado');
            }
        }//Termina if Cathedral
        if(edificio === 'Mandras'){//TERMINADO
            /*HORIZONTALES*/
                if(x<3){
                    //H1
                    //Agarra 2 (2-4 / 3-1) Si agarro (H1)
                    if((myarray[x][y] === 2) && (myarray[x][(y+1)] === 4) && (myarray[(x+1)][y] === 3) && (myarray[(x+1)][(y+1)] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();  
                        monumentoCreado = true;
                    }
                    //Agarra 4 (2-4 / 3-1) Si agarro (H1)
                    if((myarray[x][y] === 4) && (myarray[x][(y-1)] === 2) && (myarray[(x+1)][(y-1)] === 3) && (myarray[(x+1)][y] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        valoresBooleanos();  
                        monumentoCreado = true;
                    }
                    //H2
                    //Agarra 3 (3-1 / 2-4) Si agarro (H2)
                    if((myarray[x][y] === 3) && (myarray[x][(y+1)] === 1) && (myarray[(x+1)][y] === 2) && (myarray[(x+1)][(y+1)] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y+1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 1 (3-1 / 2-4) Si agarro (H2)
                    if((myarray[x][y] === 1) && (myarray[x][(y-1)] === 3) && (myarray[(x+1)][y] === 4) && (myarray[(x+1)][(y-1)] === 2)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y-1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //H3
                    //Agarra 4 (4-2 / 1-3) Si agarro (H3)
                    if((myarray[x][y] === 4) && (myarray[x][y+1] === 2) && (myarray[x+1][y] === 1) && (myarray[x+1][y+1] === 3)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y+1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 2 (4-2 / 1-3) Si agarro (H3)
                    if((myarray[x][y] === 2) && (myarray[x][y-1] === 4) && (myarray[x+1][y] === 3) && (myarray[x+1][y-1] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y-1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //H4
                    //Agarra 1 (1-3 / 4-2) Si agarro (H4)
                    if((myarray[x][y] === 1) && (myarray[x][y+1] === 3) && (myarray[x+1][y] === 4) && (myarray[x+1][y+1] === 2)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y+1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 3 (1-3 / 4-2) Si agarro (H4)
                    if((myarray[x][y] === 3) && (myarray[x][y-1] === 1) && (myarray[x+1][y] === 2) && (myarray[x+1][y-1] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y-1] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                }
                if(x>0){
                    //H1
                    //Agarra 3 (2-4 / 3-1) Si agarro (H1)
                    if((myarray[x][y] === 3 ) && (myarray[x][(y+1)] === 1) && (myarray[(x-1)][y] === 2) && (myarray[(x-1)][(y+1)] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos(); 
                        monumentoCreado = true; 
                    }
                    //Agarra 1 (2-4 / 3-1) Si agarro (H1)
                    if((myarray[x][y] === 1) && (myarray[x][(y-1)] === 3) && (myarray[(x-1)][(y-1)] === 2) && (myarray[(x-1)][y] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();  
                        monumentoCreado = true;
                    }
                    //H2
                    //Agarra 2 (3-1 / 2-4) Si agarro (H2)
                    if((myarray[x][y] === 2) && (myarray[x][(y+1)] === 4) && (myarray[(x-1)][y] === 3) && (myarray[(x-1)][(y+1)] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y+1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 4 (3-1 / 2-4) Si agarro (H2)
                    if((myarray[x][y] === 4) && (myarray[x][(y-1)] === 2) && (myarray[(x-1)][(y-1)] === 3) && (myarray[(x-1)][y] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y-1] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //H3
                    //Agarra 3 (4-2 / 1-3) Si agarro (H3)
                    if((myarray[x][y] === 3) && (myarray[x][y-1] === 1) && (myarray[x-1][y] === 2) && (myarray[x-1][y-1] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y-1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 1 (4-2 / 1-3) Si agarro (H3)
                    if((myarray[x][y] === 1) && (myarray[x][y+1] === 1) && (myarray[x-1][y] === 4) && (myarray[x-1][y+1] === 2)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y+1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //H4
                    //Agarra 4 (1-3 / 4-2) Si agarro (H4)
                    if((myarray[x][y] === 4) && (myarray[x][y+1] === 2) && (myarray[x-1][y] === 1) && (myarray[x-1][y+1] === 3)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y+1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 2 (1-3 / 4-2) si agarro (H4)
                    if((myarray[x][y] === 2) && (myarray[x][y-1] === 4) && (myarray[x-1][y] === 3) && (myarray[x-1][y-1] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][y-1] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                }
            /*VERTICALES*/
                if(x < 3 && y < 3){
                    //V1
                    //Agarra 2 (2/4 - 3/1) Si agarro (V1)
                    if((myarray[x][y] === 2) && (myarray[x][y+1] === 3) && (myarray[x+1][y] === 4) && (myarray[x+1][y+1] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 4 (2/4 - 3/1) Si agarro (V1)
                    if((myarray[x][y] === 4) && (myarray[x][y+1] === 1) && (myarray[x-1][y] === 2) && (myarray[x-1][y+1] === 3)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    if((x>0) && (y>0)){
                        //Agarra 1 (2/4 - 3/1) Si agarro (V1)
                        if((myarray[x][y] === 1) && (myarray[x][y-1] === 4) && (myarray[x-1][y] === 3) && (myarray[x-1][y-1] === 2)){
                            myarray[x][y] = 'M'; 
                            document.getElementById(id).src = mandras;
                            myarray[x][(y-1)] = 0;
                            myarray[(x-1)][y] = 0;
                            myarray[(x-1)][(y-1)] = 0;
                            document.getElementById('c'+x+(y-1)).src = tablero;
                            document.getElementById('c'+(x-1)+y).src = tablero;
                            document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                            valoresBooleanos();
                            monumentoCreado = true;
                        }
                    }
                    //Agarra 3 (2/4 - 3/1) Si agarro (V1)
                    if((myarray[x][y] === 3) && (myarray[x][y-1] === 2) && (myarray[x+1][y] === 1) && (myarray[x+1][y-1] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
    
                    //V2
                    //Agarra 3 (3/1 - 2/4) Si agarro (V2)
                    if((myarray[x][y] === 3) && (myarray[x][y+1] === 2) && (myarray[x+1][y] === 1) && (myarray[x+1][y+1] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 2 (3/1 - 2/4) Si agarro (V2)
                    if((myarray[x][y] === 2) && (myarray[x][y-1] === 3) && (myarray[x+1][y] === 4) && (myarray[x+1][y-1] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 1 (3/1 - 2/4) Si agarro (V2)
                    if((myarray[x][y] === 1) && (myarray[x][y+1] === 4) && (myarray[x-1][y] === 3) && (myarray[x-1][y+1] === 2)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 4 (3/1 - 2/4) Si agarro (V2)
                    if((myarray[x][y] === 4) && (myarray[x][y-1] === 1) && (myarray[x-1][y] === 2) && (myarray[x-1][y-1] === 3)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
    
                    //V3
                    //Agarra 4 (4/2 - 1/3) Si agarra (V3)
                    if((myarray[x][y] === 4) && (myarray[x][y+1] === 1) && (myarray[x+1][y] === 2) && (myarray[x+1][y+1] === 3)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 1 (4/2 - 1/3) Si agarra (V3)
                    if((myarray[x][y] === 1) && (myarray[x][y-1] === 4) && (myarray[x+1][y] === 3) && (myarray[x+1][y-1] === 2)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 2 (4/2 - 1/3) Si agarra (V3)
                    if((myarray[x][y] === 2) && (myarray[x][y+1] === 3) && (myarray[x-1][y] === 4) && (myarray[x-1][y+1] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 3 (4/2 - 1/3) Si agarro (V3)
                    if((myarray[x][y] === 3) && (myarray[x][y-1] === 2) && (myarray[x-1][y] === 1) && (myarray[x-1][y-1] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    
                    //V4
                    //Agarra 1 (4/2 - 1/3) Si agarra (V4)
                    if((myarray[x][y] === 1) && (myarray[x][y+1] === 4) && (myarray[x+1][y] === 3) && (myarray[x+1][y+1] === 2)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 4 (4/2 - 1/3) Si agarro (V4)
                    if((myarray[x][y] === 4) && (myarray[x][y-1] === 1) && (myarray[x+1][y] === 2) && (myarray[x+1][y-1] === 3)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x+1)][y] = 0;
                        myarray[(x+1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x+1)+y).src = tablero;
                        document.getElementById('c'+(x+1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 3 (4/2 - 1/3) Si agarra (V4)
                    if((myarray[x][y] === 3) && (myarray[x][y+1] === 2) && (myarray[x-1][y] === 1) && (myarray[x-1][y+1] === 4)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y+1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y+1)] = 0;
                        document.getElementById('c'+x+(y+1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y+1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                    //Agarra 2 (4/2 - 1/3) Si agarra(V4)
                    if((myarray[x][y] === 2) && (myarray[x][y-1] === 3) && (myarray[x-1][y] === 4) && (myarray[x-1][y-1] === 1)){
                        myarray[x][y] = 'M'; 
                        document.getElementById(id).src = mandras;
                        myarray[x][(y-1)] = 0;
                        myarray[(x-1)][y] = 0;
                        myarray[(x-1)][(y-1)] = 0;
                        document.getElementById('c'+x+(y-1)).src = tablero;
                        document.getElementById('c'+(x-1)+y).src = tablero;
                        document.getElementById('c'+(x-1)+(y-1)).src = tablero;
                        valoresBooleanos();
                        monumentoCreado = true;
                    }
                } 
                if(validarEdificio === false){
                    alert('Elige un campo adecuado');
                }
        }//Termina if Mandras
    }
        else{
        llenarCampo(x,y,id);
        }
    }

function valoresBooleanos(){
        edificioCreado = false;
        hayEdificioAmarrillo = false;
        hayEdificioAzul = false;
        hayEdificioChillante = false;
        hayEdificioGris = false;
        hayEdificioLila = false;
        hayEdificioNaranja = false;
        hayEdificioNegro = false;
        hayEdificioVerde = false;
        validarEdificio = true;
        document.getElementById('gris').disabled = true;
        document.getElementById('gris').style.borderColor = 'white';
        document.getElementById('azul').disabled = true;
        document.getElementById('azul').style.borderColor = 'white';
        document.getElementById('naranja').disabled = true;
        document.getElementById('naranja').style.borderColor = 'white';
        document.getElementById('amarilla').disabled = true;
        document.getElementById('amarilla').style.borderColor = 'white';
        document.getElementById('verde').disabled = true;
        document.getElementById('verde').style.borderColor = 'white';
        document.getElementById('chillante').disabled = true;
        document.getElementById('chillante').style.borderColor = 'white';
        document.getElementById('negra').disabled = true;
        document.getElementById('negra').style.borderColor = 'white';
        document.getElementById('lila').disabled = true;
        document.getElementById('lila').style.borderColor = 'white';
}
function usarMonumento(){
    if(monumentoElegido === 'Cathedral'){
        cathedralOfCatherina();
    }
    else if(monumentoElegido === 'Mandras'){       
        mandrasPlace();
    }
}

/*Puntuacion*/
function contabilizar(){
    var catherina = false;
    var mandras = false;
    var puntosTotales = 0;
    var puntosnegativos = 0;
    var nivel = '';
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'CC'){
                catherina = true;
            }
            else if(myarray[x][y] === 'M'){
                mandras = true;
            }
        }
    }
    if(catherina === true){
        puntosTotales = contabilizarAbbey()+contabilizarFarm()+contabilizarTavern()+contabilizarwell()+contabilizarTradingPost()+contabilizarBackery();
        document.getElementById('puntos').innerHTML = puntosTotales;
    }
    else if(mandras === true){
        for(let x = 0; x < myarray.length; x++){
            for(let y = 0; y < myarray.length; y++){
                if(myarray[x][y] != 'M' && myarray[x][y] != 'W' && myarray[x][y] != 'C'&& myarray[x][y] != 'T' && myarray[x][y] != 'A' && myarray[x][y] != 'TP' && myarray[x][y] != 'F' && myarray[x][y] != 'B'){
                    puntosnegativos --;
                }
            }
        }
        puntosTotales = puntosnegativos+contabilizarAbbey()+contabilizarFarm()+contabilizarTavern()+contabilizarwell()+contabilizarTradingPost()+contabilizarBackery();
        document.getElementById('puntos').innerHTML = puntosTotales;
    }
    else{
        for(let x = 0; x < myarray.length; x++){
            for(let y = 0; y < myarray.length; y++){
                if(myarray[x][y] != 'W' && myarray[x][y] != 'C'&& myarray[x][y] != 'T' && myarray[x][y] != 'A' && myarray[x][y] != 'TP' && myarray[x][y] != 'F' && myarray[x][y] != 'B'){
                    puntosnegativos --;
                }
            }
        }
        puntosTotales = puntosnegativos+contabilizarAbbey()+contabilizarFarm()+contabilizarTavern()+contabilizarwell()+contabilizarTradingPost()+contabilizarBackery();
        if(puntosTotales <= 9){
            nivel = 'Aspirante a arquitecto';
        }
        else if(puntosTotales <= 17 && puntosTotales >= 10){
            nivel = 'Aprendiz de constructor';
        }
        else if(puntosTotales <= 24 && puntosTotales >= 18){
            nivel = 'Carpintero';
        }
        else if(puntosTotales <= 31 && puntosTotales >= 25){
            nivel = 'Ingeniero';
        }
        else if(puntosTotales <= 37 && puntosTotales >= 32){
            nivel = 'Urbanista';
        }
        else if(puntosTotales >= 38){
            nivel = 'Maestro arquitecto';
        }
    } 
    document.getElementById('puntos').innerHTML = 'Has terminado la partida tu puntaje es de: '+puntosTotales;
    document.getElementById('nivel').innerHTML = 'Tu nivel es de un ' +nivel+'.';
    return puntosTotales; 
}
function contabilizarwell(){
    var well = 0;
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'W'){
                if(x < 3){
                    if(myarray[x+1][y] === 'C'){
                        well+=1;
                    }
                }
                if(x > 0){
                    if(myarray[x-1][y] === 'C'){
                        well++;
                    }
                }
                if(y < 3){
                    if(myarray[x][y+1] === 'C'){
                        well++;
                    }
                }
                if(y > 0){
                    if(myarray[x][y-1] === 'C'){
                        well++;
                    }
                }
            }
        }
    }
    return well;
}
function contabilizarTavern(){
    var tavern = 0;
    var puntos = 0;
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'T'){
                tavern++;
            }
        }
    }
    switch(tavern){
        case 1:
            puntos = 2;
            break;
        case 2: 
            puntos = 5;
            break;
        case 3:
            puntos = 9;
            break;
        case 4:
            puntos = 14;
            break;
        case 5:
            puntos = 20;
            break;
    }
    return puntos;
}
function contabilizarAbbey(){
    var abbey = 0;
    var hayAbbey = false;
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'A'){
                if(x < 3){
                    if(myarray[x+1][y] === 'T' || myarray[x+1][y] === 'B' || myarray[x+1][y] === 'TP'){
                        abbey=0;
                        hayAbbey = true;
                    }
                }
                if(x > 0){
                    if(myarray[x-1][y] === 'T' || (myarray[x-1][y] === 'B') || (myarray[x-1][y] === 'TP')){
                        abbey=0;
                        hayAbbey = true;
                    }
                }
                if(y < 3){
                    if(myarray[x][y+1] === 'T'|| (myarray[x-1][y] === 'B') || (myarray[x-1][y] === 'TP')){
                        abbey=0;
                        hayAbbey = true;
                    }
                }
                if(y > 0){
                    if(myarray[x][y-1] === 'T' || myarray[x][y-1] === 'B' | myarray[x][y-1] === 'TP'){
                        abbey=0;
                        hayAbbey = true;
                    }
                }
                if(hayAbbey === false){
                    abbey +=3;
                }
            }
        }
    }
    return abbey;
}
function contabilizarTradingPost(){
    var trading = 0;
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'TP'){
                trading++;
            }
        }
    }
    return trading;
}
function contabilizarFarm(){
    var farm = 0;
    var cottage = 0;
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'F'){
                farm += 4;
            }
        }
    }
    if(farm > 0){
        for(let x = 0; x < myarray.length; x++){
            for(let y = 0; y < myarray.length; y++){
                if(myarray[x][y] === 'C'){
                    cottage += 3;
                    farm --;
                }
            }
        }
    }
    return cottage;
}

function contabilizarBackery(){
    var backery = 0;
    var hayBackery = false;
    for(let x = 0; x < myarray.length; x++){
        for(let y = 0; y < myarray.length; y++){
            if(myarray[x][y] === 'B'){
                if(x < 3){
                    if(myarray[x+1][y] === 'F' || myarray[x+1][y] === 'TP'){
                        backery=3;
                        hayBackery = true;
                    }
                }
                if(x > 0){
                    if(myarray[x+1][y] === 'F' || myarray[x+1][y] === 'TP'){
                        backery=3;
                        hayBackery = true;
                    }
                }
                if(y < 3){
                    if(myarray[x+1][y] === 'F' || myarray[x+1][y] === 'TP'){
                        backery=3;
                        hayBackery = true;
                    }
                }
                if(y > 0){
                    if(myarray[x+1][y] === 'F' || myarray[x+1][y] === 'TP'){
                        backery=3;
                        hayBackery = true;
                    }
                }
                if(hayBackery === false){
                    backery = 0;
                }
            }
        }
    }
    return backery;
}